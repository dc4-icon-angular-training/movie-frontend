import { AdminGuard } from '@/modules/admin/admin.guard';
import { NotFoundComponent } from '@/components/not-found/not-found.component';
import { MainLayoutComponent } from '@/components/main-layout/main-layout.component';
import { AuthLayoutComponent } from '@/components/auth-layout/auth-layout.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminLayoutComponent } from '@/components/admin-layout/admin-layout.component';

const routes: Routes = [
  {
    path: '404',
    title: 'Page not found',
    component: NotFoundComponent,
  },
  {
    path: 'admin',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('@/modules/admin/admin.module').then((m) => m.AdminModule),
      },
    ],
    canLoad: [AdminGuard],
    canActivate: [AdminGuard],
  },
  {
    path: 'auth',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('@/modules/auth/auth.module').then((m) => m.AuthModule),
      },
    ],
  },
  {
    path: 'movies',
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('@/modules/movie/movie.module').then((m) => m.MovieModule),
      },
    ],
  },
  { path: '', redirectTo: 'movies', pathMatch: 'full' },
  { path: '**', redirectTo: '404', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
