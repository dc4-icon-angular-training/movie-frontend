import { MovieType } from '@/modules/movie/models/movie.model';
import { TypeService } from '@/modules/movie/services/type.service';
import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { map, mergeMap, of, Observable, pipe } from 'rxjs';

@Component({
  selector: 'app-type-modal',
  templateUrl: './type-modal.component.html',
  styleUrls: ['./type-modal.component.scss'],
})
export class TypeModalComponent {
  form!: FormGroup;

  constructor(
    private modalRef: MatDialogRef<TypeModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { movieType: MovieType },
    private readonly fb: FormBuilder,
    private readonly typeService: TypeService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      id: ['0'],
      name: ['', Validators.required],
    });

    if (this.data.movieType) {
      this.form.patchValue(this.data.movieType);
    }
  }

  onNoClick(): void {
    this.modalRef.close();
  }

  submit() {
    if (this.form.valid) {
      of(this.form.value as MovieType)
        .pipe(
          mergeMap((movieType) => {
            if (this.data.movieType) {
              return this.typeService.updateMovieType(movieType);
            }
            return this.typeService.addMovieType(movieType);
          })
        )
        .subscribe(() => {
          this.modalRef.close({ movieType: this.form.value });
        });
    }
  }
}
