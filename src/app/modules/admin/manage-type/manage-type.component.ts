import { map, Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { MovieType } from '@/modules/movie/models/movie.model';
import { MatDialog } from '@angular/material/dialog';
import { TypeService } from '@/modules/movie/services/type.service';
import { TypeModalComponent } from '@/modules/admin/type-modal/type-modal.component';

@Component({
  selector: 'app-manage-type',
  templateUrl: './manage-type.component.html',
  styleUrls: ['./manage-type.component.scss'],
})
export class ManageTypeComponent implements OnInit {
  types: MovieType[] = [];

  displayedColumns = ['index', 'name', 'action'];

  constructor(
    private readonly typeService: TypeService,
    private readonly dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getListMovieType();
  }

  getListMovieType() {
    this.typeService.getListMovieType().subscribe((data) => {
      this.types = data;
    });
  }

  handleDelete(movieId: number) {
    if (confirm('Are you sure?')) {
      this.typeService.deleteMovieType(movieId).subscribe(() => {
        alert('Type deleted successfully!!!');
        this.getListMovieType();
      });
    }
  }

  openModal(movieType?: MovieType): void {
    const modalRef = this.dialog.open(TypeModalComponent, {
      data: { movieType },
    });
    modalRef
      .afterClosed()
      .pipe(
        map((result) => {
          if (result?.movieType) {
            this.getListMovieType();
          }
        })
      )
      .subscribe();
  }
}
