import { Observable } from 'rxjs';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss'],
})
export class UploadImageComponent implements OnInit {
  @Input() imageURL: string = '';
  @Output() imageChange = new EventEmitter<File>();

  selectedFile?: File;

  selectedFileName: string = '';

  preview: string = '';

  ngOnInit(): void {
    if (this.imageURL) {
      this.selectedFileName = this.imageURL.split('/').slice(-1)[0];
      this.preview = this.imageURL;
    }
  }

  selectFile(event: any): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.selectedFile = fileList[0];
      this.preview = '';
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.preview = e.target.result;
      };
      reader.readAsDataURL(this.selectedFile);
      this.selectedFileName = this.selectedFile.name;

      this.imageChange.emit(this.selectedFile)
    }
  }
}
