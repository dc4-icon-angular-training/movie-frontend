import { ManageMovieComponent } from '@/modules/admin/manage-movie/manage-movie.component';
import { Routes } from '@angular/router';
import { ManageTypeComponent } from '@/modules/admin/manage-type/manage-type.component';
export const adminRoutes: Routes = [
  {
    path: 'movies',
    title: 'Manage Movie',
    component: ManageMovieComponent,
  },
  {
    path: 'types',
    title: 'Manage type',
    component: ManageTypeComponent,
  },
  {
    path: '',
    redirectTo: 'movies',
    pathMatch: 'full',
  },
];
