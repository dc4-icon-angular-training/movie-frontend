import { MatInputModule } from '@angular/material/input';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageMovieComponent } from '@/modules/admin/manage-movie/manage-movie.component';
import { RouterModule } from '@angular/router';
import { adminRoutes } from '@/modules/admin/admin.routes';
import { MovieService } from '@/modules/movie/services/movie.service';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MovieModalComponent } from '@/modules/admin/movie-modal/movie-modal.component';
import { UploadImageComponent } from '@/modules/admin/upload-image/upload-image.component';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { TypeService } from '@/modules/movie/services/type.service';
import { ManageTypeComponent } from '@/modules/admin/manage-type/manage-type.component';
import { TypeModalComponent } from '@/modules/admin/type-modal/type-modal.component';

@NgModule({
  declarations: [
    ManageMovieComponent,
    MovieModalComponent,
    UploadImageComponent,
    ManageTypeComponent,
    TypeModalComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(adminRoutes),
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCardModule,
    MatListModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatProgressBarModule,
  ],
  providers: [MovieService, TypeService],
  exports: [UploadImageComponent],
})
export class AdminModule {}
