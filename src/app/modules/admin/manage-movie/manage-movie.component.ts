import { mergeMap, take } from 'rxjs/operators';
import { Movie, Pagination } from '@/modules/movie/models/movie.model';
import { MovieService } from '@/modules/movie/services/movie.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { map, Observable, of, shareReplay, delay, catchError } from 'rxjs';

import { MovieModalComponent } from '../movie-modal/movie-modal.component';
import { tap } from 'rxjs';

@Component({
  selector: 'app-manage-movie',
  templateUrl: './manage-movie.component.html',
  styleUrls: ['./manage-movie.component.scss'],
})
export class ManageMovieComponent implements OnInit {
  movies: Pagination<Movie> | null = null;
  pageSize: number = 12;
  pageIndex: number = 0;
  length: number = 0;

  displayedColumns = ['name', 'description', 'year', 'duration', 'action'];

  constructor(
    private readonly movieService: MovieService,
    private readonly dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getMovieList();
  }

  getMovieList(page = 0, size = 12) {
    this.movieService.getListMovie(page, size).subscribe((data) => {
      this.movies = data;
      this.pageSize = data.size;
      this.pageIndex = data.page;
      this.length = data.size * data.totalPages;
    });
  }

  handlePageEvent(event: PageEvent) {
    this.getMovieList(event.pageIndex, event.pageSize);
  }

  handleDelete(movieId: number) {
    if (confirm('Are you sure?')) {
      this.movieService.deleteMovie(movieId).subscribe(() => {
        alert('Movie deleted successfully!!!');
        this.getMovieList(this.pageIndex, this.pageSize);
      });
    }
  }

  openModal(slug: string): void {
    const modalRef = this.dialog.open(MovieModalComponent, {
      data: { slug },
    });

    modalRef
      .afterClosed()
      .pipe(
        map((result) => {
          if (result?.movie) {
            this.getMovieList(this.pageIndex, this.pageSize);
          }
        })
      )
      .subscribe();
  }
}
