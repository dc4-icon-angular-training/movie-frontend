import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment,
} from '@angular/router';
import { catchError, map, Observable } from 'rxjs';
import { AuthService } from '@/modules/auth/services/auth.service';
import { User } from '../auth/models/auth.model';
import { handleError } from '@/settings/helper';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate, CanLoad {
  constructor(
    private router: Router,
    private readonly authService: AuthService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean {
    const user = this.authService.userValue;

    if (!localStorage.getItem('movie-accessToken')) {
      this.router.navigate(['/auth', 'login']);
      return false;
    }
    if (user) {
      return this.authService.user.pipe(map(this.checkUserAccess));
    }

    return this.authService.getUserProfile().pipe(
      map(this.checkUserAccess),
      catchError((err) => {
        this.router.navigate(['/']);
        return handleError(err);
      })
    );
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | boolean {
    return this.authService.user.pipe(map(this.checkUserAccess));
  }

  checkUserAccess(user: User | null) {
    if (user?.role !== 'admin') {
      alert('No permission');
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}
