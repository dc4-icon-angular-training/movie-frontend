import { Movie } from '@/modules/movie/models/movie.model';
import { MovieService } from '@/modules/movie/services/movie.service';
import { Component, Inject, AfterContentChecked, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { mergeMap, tap } from 'rxjs';
import { Observable, of, switchMap, shareReplay, map } from 'rxjs';

@Component({
  selector: 'app-movie-modal',
  templateUrl: './movie-modal.component.html',
  styleUrls: ['./movie-modal.component.scss'],
})
export class MovieModalComponent implements OnInit {
  form!: FormGroup;
  movie!: Observable<Movie>;

  imageFile: File | null = null;

  constructor(
    private modalRef: MatDialogRef<MovieModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { slug: string },
    private readonly fb: FormBuilder,
    private readonly movieService: MovieService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      id: ['0'],
      name: ['', Validators.required],
      description: ['', Validators.required],
      duration: ['', Validators.required],
      image: [''],
      year: ['', Validators.required],
      typeId: [1],
      cast: [''],
    });

    this.movie = this.data.slug
      ? this.movieService.getMovieDetails(this.data.slug).pipe(
          tap((movie) => {
            this.form.patchValue(movie);
            return movie;
          })
        )
      : of({} as Movie);
  }

  onNoClick(): void {
    this.modalRef.close();
  }

  submit() {
    if (this.form.valid) {
      of(this.form.value as Movie)
        .pipe(
          mergeMap((movie) => {
            if (this.data.slug) {
              return this.movieService.updateMovie(movie);
            }
            return this.movieService.addMovie(movie);
          }),
          mergeMap((movie) => {
            if (this.imageFile) {
              return this.movieService.uploadMovieImage(
                movie.id,
                this.imageFile
              );
            }
            return of(movie);
          })
        )
        .subscribe(() => {
          this.modalRef.close({ movie: this.form.value });
        });
    }
  }

  onImageChange(file: File) {
    this.imageFile = file;
  }
}
