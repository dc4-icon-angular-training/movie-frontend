import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieListComponent } from '@/modules/movie/movie-list/movie-list.component';
import { MovieDetailsComponent } from '@/modules/movie/movie-details/movie-details.component';
import { movieRoutes } from '@/modules/movie/movie.routes';
import { RouterModule } from '@angular/router';
import { MovieCardComponent } from '@/modules/movie/components/movie-card/movie-card.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MovieService } from '@/modules/movie/services/movie.service';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { SortMoviePipe } from '@/modules/movie/pipes/sort-movie.pipe';
import { TypeService } from '@/modules/movie/services/type.service';

@NgModule({
  declarations: [
    MovieListComponent,
    MovieDetailsComponent,
    MovieCardComponent,
    SortMoviePipe,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(movieRoutes),
    MatButtonModule,
    MatPaginatorModule,
    MatSelectModule,
    FormsModule,
  ],
  providers: [MovieService, TypeService],
})
export class MovieModule {}
