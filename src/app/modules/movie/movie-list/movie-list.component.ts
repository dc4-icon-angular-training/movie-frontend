import { SortOrder, SortField } from './../models/movie.model';
import { UIComponent } from '@/abstracts/ui-component';
import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { MovieService } from '@/modules/movie/services/movie.service';
import { Observable } from 'rxjs';
import { Movie, Pagination } from '@/modules/movie/models/movie.model';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
})
export class MovieListComponent implements OnInit {
  movies: Pagination<Movie> | null = null;
  pageSize: number = 12;
  pageIndex: number = 0;
  length: number = 0;

  sortOrder: SortOrder = 0;
  sortField?: SortField;

  constructor(private readonly movieService: MovieService) {}

  ngOnInit(): void {
    this.getMovieList();
  }

  getMovieList(page = 0, size = 12) {
    this.movieService.getListMovie(page, size).subscribe((data) => {
      this.movies = data;
      this.pageSize = data.size;
      this.pageIndex = data.page;
      this.length = data.size * data.totalPages;
    });
  }

  handlePageEvent(event: PageEvent) {
    this.getMovieList(event.pageIndex, event.pageSize);
  }
}
