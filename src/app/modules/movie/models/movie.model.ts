export interface Movie {
  readonly id: string;
  name: string;
  description: string;
  year: string;
  image: string;
  duration: string;
  cast: string;
  slug?: string;
  typeId: number;
}

export interface MovieType {
  id: string;
  name: string;
  slug: string;
}

export interface Pagination<T> {
  data: T[];
  page: number;
  size: number;
  totalPages: number;
}

export type SortOrder = -1 | 0 | 1;
export type SortField = 'name' | 'year';
