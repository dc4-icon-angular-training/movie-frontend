import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, pluck, switchMap } from 'rxjs';
import { Movie } from '../models/movie.model';
import { MovieService } from '../services/movie.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
})
export class MovieDetailsComponent implements OnInit {
  movie$: Observable<Movie> | undefined;

  constructor(
    private readonly movieService: MovieService,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.movie$ = this.route.params.pipe(
      pluck('slug'),
      switchMap((slug) => this.movieService.getMovieDetails(slug))
    );
  }
}
