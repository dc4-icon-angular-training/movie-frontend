import { UIService } from '@/abstracts/ui-service';
import { Injectable } from '@angular/core';
import { Movie, Pagination } from '@/modules/movie/models/movie.model';
import { map, Observable, of, shareReplay, delay, catchError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { handleError } from '@/settings/helper';

@Injectable()
export class MovieService {
  constructor(private http: HttpClient) {}

  getListMovie(page = 0, size = 12): Observable<Pagination<Movie>> {
    const params = new HttpParams({
      fromObject: {
        page,
        size,
      },
    });

    return this.http.get<Pagination<Movie>>('/movies', { params }).pipe(
      map((data: Pagination<Movie>) => {
        return data;
      }),
      catchError(handleError)
    );
  }

  getMovieDetails(slug: string): Observable<Movie> {
    return this.http
      .get<Movie>(`/movies/${slug}`)
      .pipe(catchError(handleError));
  }

  addMovie(payload: Movie): Observable<Movie> {
    return this.http
      .post<Movie>('/admin/movies', payload)
      .pipe(catchError(handleError));
  }

  updateMovie(payload: Movie): Observable<Movie> {
    return this.http
      .put<Movie>(`/admin/movies/${payload.id}`, payload)
      .pipe(catchError(handleError));
  }

  deleteMovie(movieId: number): Observable<unknown> {
    return this.http
      .delete(`/admin/movies/${movieId}`)
      .pipe(catchError(handleError));
  }

  uploadMovieImage(movieId: string, file: File) {
    console.log({ movieId, file });
    const formData = new FormData();
    formData.append('image', file);

    console.log({ formData });

    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'multipart/form-data');
    headers = headers.append('enctype', 'multipart/form-data');

    return this.http
      .put(`/admin/movies/${movieId}/image`, formData, {
        headers,
      })
      .pipe(catchError(handleError));
  }
}
