import { Observable, catchError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MovieType } from '@/modules/movie/models/movie.model';
import { handleError } from '@/settings/helper';

@Injectable()
export class TypeService {
  constructor(private http: HttpClient) {}

  getListMovieType(): Observable<MovieType[]> {
    return this.http.get<MovieType[]>('/types').pipe(catchError(handleError));
  }

  addMovieType(payload: MovieType): Observable<MovieType> {
    return this.http
      .post<MovieType>('/admin/types', payload)
      .pipe(catchError(handleError));
  }

  updateMovieType(payload: MovieType): Observable<MovieType> {
    return this.http
      .put<MovieType>(`/admin/types/${payload.id}`, payload)
      .pipe(catchError(handleError));
  }

  deleteMovieType(movieId: number): Observable<unknown> {
    return this.http
      .delete(`/admin/types/${movieId}`)
      .pipe(catchError(handleError));
  }
}
