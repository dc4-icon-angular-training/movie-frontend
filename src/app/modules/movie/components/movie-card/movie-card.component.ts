import { UIComponent } from '@/abstracts/ui-component';
import { Component, Input } from '@angular/core';
import { Movie } from '@/modules/movie/models/movie.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss'],
})
export class MovieCardComponent {
  @Input() movie: Movie = {} as Movie;

  constructor(private readonly router: Router) {}

  onVieDetailClick(slug: string) {
    this.router.navigate(['/movies', slug]);
  }
}
