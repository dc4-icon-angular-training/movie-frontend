import { RegisterComponent } from '@/modules/auth/register/register.component';
import { LoginComponent } from '@/modules/auth/login/login.component';
import { Routes } from '@angular/router';
import { MovieListComponent } from '@/modules/movie/movie-list/movie-list.component';
import { MovieDetailsComponent } from '@/modules/movie/movie-details/movie-details.component';
export const movieRoutes: Routes = [
  {
    path: '',
    title: 'Movies',
    component: MovieListComponent,
  },
  {
    path: ':slug',
    title: 'Details',
    component: MovieDetailsComponent,
  },
];
