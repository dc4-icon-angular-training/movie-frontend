import { SortField, SortOrder } from './../models/movie.model';
import { Pipe, PipeTransform } from '@angular/core';
import { Movie } from '../models/movie.model';

@Pipe({
  name: 'sortMovie',
})
export class SortMoviePipe implements PipeTransform {
  transform(
    movies: Movie[],
    sortOrder: SortOrder = 0,
    sortField?: SortField
  ): Movie[] {
    if (!sortField || sortOrder === 0) {
      return movies;
    }
    return movies.sort((mv1, mv2) => {
      if (sortOrder === -1)
        return mv1[sortField] < mv2[sortField]
          ? -1
          : mv1[sortField] > mv2[sortField]
          ? 1
          : 0;
      return mv1[sortField] > mv2[sortField]
        ? -1
        : mv1[sortField] < mv2[sortField]
        ? 1
        : 0;
    });
  }
}
