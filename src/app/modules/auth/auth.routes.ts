import { RegisterComponent } from '@/modules/auth/register/register.component';
import { LoginComponent } from '@/modules/auth/login/login.component';
import { Routes } from '@angular/router';
export const authRoutes: Routes = [
  {
    path: 'login',
    title: 'Login',
    component: LoginComponent,
  },
  {
    path: 'register',
    title: 'Register',
    component: RegisterComponent,
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
];
