import { FormBuilder, FormGroup } from '@angular/forms';
import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { RegisterPayload } from '../models/auth.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  registerForm: FormGroup = this.fb.group({
    name: '',
    username: '',
    password: '',
  });

  errorMsg: string = '';

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  register = () => {
    console.log('register: ', this.registerForm.value);
    this.authService
      .register(this.registerForm.value as RegisterPayload)
      .subscribe({
        next: () => {
          this.errorMsg = '';
          this.router.navigate(['/auth', 'login']);
        },
        error: (err) => {
          console.log(err);
        },
      });
  };
}
