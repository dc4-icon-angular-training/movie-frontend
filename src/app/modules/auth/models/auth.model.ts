export interface User {
  id: string;
  name: string;
  username: string;
  avatar: string;
  role: 'admin' | 'participate';
}

export interface RegisterPayload {
  name: string;
  username: string;
  password: string;
}

export type LoginPayload = Omit<RegisterPayload, 'name'>;

export interface AuthResponse {
  user: User;
  accessToken: string;
}
