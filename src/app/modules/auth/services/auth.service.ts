import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  LoginPayload,
  User,
  AuthResponse,
  RegisterPayload,
} from '@/modules/auth/models/auth.model';
import { HttpClient } from '@angular/common/http';
import { handleError } from '@/settings/helper';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private userSubject: BehaviorSubject<User | null> =
    new BehaviorSubject<User | null>(null);

  public user: Observable<User | null>;

  constructor(private http: HttpClient, private router: Router) {
    if (localStorage.getItem('movie-accessToken')) {
      console.log('ssss');
      this.getUserProfile().subscribe();
    }
    this.user = this.userSubject.asObservable();
  }

  get currentUser(): Observable<User | null> {
    return this.user;
  }

  get userValue(): User | null {
    return this.userSubject.getValue();
  }

  login(payload: LoginPayload): Observable<AuthResponse> {
    return this.http.post<AuthResponse>('/auth/login', payload).pipe(
      map((data: AuthResponse) => {
        localStorage.setItem('movie-accessToken', data.accessToken);
        this.userSubject.next(data.user);
        this.user = this.userSubject.asObservable();
        return data;
      }),
      catchError(handleError)
    );
  }

  register(payload: RegisterPayload): Observable<AuthResponse> {
    return this.http
      .post<AuthResponse>('/auth/register', payload)
      .pipe(catchError(handleError));
  }

  logout(): void {
    localStorage.removeItem('movie-accessToken');
    this.userSubject.next(null);
    this.router.navigate(['/movies']);
  }

  getUserProfile(): Observable<User> {
    return this.http.get<User>('/me').pipe(
      map((data: User) => {
        this.userSubject.next(data);
        this.user = this.userSubject.asObservable();
        return data;
      }),
      catchError(handleError)
    );
  }

  uploadProfile(payload: User): Observable<User> {
    return this.http.put<User>('/me', payload).pipe(catchError(handleError));
  }

  uploadAvatar(file: File): Observable<string> {
    const formData: FormData = new FormData();
    formData.append('image', file);
    return this.http
      .put<string>('/me/image', formData)
      .pipe(catchError(handleError));
  }
}
