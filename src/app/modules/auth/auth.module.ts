import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '@/modules/auth/login/login.component';
import { RegisterComponent } from '@/modules/auth/register/register.component';
import { RouterModule } from '@angular/router';
import { authRoutes } from '@/modules/auth/auth.routes';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [LoginComponent, RegisterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(authRoutes),
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
  ],
})
export class AuthModule {}
