import { HttpHeaders } from '@angular/common/http';
import constants from './constants';

const settings = {
  api: {
    baseURL: 'http://localhost:8080',
    httpOptions: {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    },

    endpoints: {
      auth: {
        url: '/auth',
      },
    },
    routes: {
      login: { path: '/login', roles: [] },
    },
  },
  constants,
};

export { settings };
