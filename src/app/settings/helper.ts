import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

export const handleError = (error: HttpErrorResponse) => {
  return throwError(error?.error?.error || error?.message);
};
