const MAXIMUM_IMAGE_SIZE_MB = 20;

export default {
  MAXIMUM_IMAGE_SIZE_MB,
};
