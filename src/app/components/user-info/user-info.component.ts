import { AuthService } from '@/modules/auth/services/auth.service';
import { AfterContentChecked } from '@angular/core';
import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { User } from '@/modules/auth/models/auth.model';
import { _MatMenuTriggerBase } from '@angular/material/menu';
import { MatDialog } from '@angular/material/dialog';
import { UserModalComponent } from '@/components/user-modal/user-modal.component';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss'],
})
export class UserInfoComponent implements OnInit, AfterContentChecked {
  user: User | null = null;
  constructor(private authService: AuthService, private dialog: MatDialog) {}

  ngOnInit(): void {}

  ngAfterContentChecked(): void {
    this.user = this.authService.userValue;
  }

  logout() {
    if (confirm('Do you want to logout?')) {
      this.authService.logout();
    }
  }

  openUserModal() {
    const dialogRef = this.dialog.open(UserModalComponent, {
      restoreFocus: false,
    });
    dialogRef.afterClosed().subscribe();
  }
}
