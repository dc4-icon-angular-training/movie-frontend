import { User } from '@/modules/auth/models/auth.model';
import { AuthService } from '@/modules/auth/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { mergeMap, Observable, of, tap } from 'rxjs';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss'],
})
export class UserModalComponent implements OnInit {
  form!: FormGroup;
  user$!: Observable<User | null>;
  imageFile: File | null = null;

  constructor(
    private modalRef: MatDialogRef<UserModalComponent>,
    private readonly fb: NonNullableFormBuilder,
    private readonly authService: AuthService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      id: ['0'],
      name: ['', Validators.required],
    });

    this.user$ = this.authService.currentUser.pipe(
      tap((user) => this.form.patchValue({ id: user?.id, name: user?.name }))
    );
  }

  onImageChange(file: File) {
    this.imageFile = file;
  }
  onNoClick(): void {
    this.modalRef.close();
  }
  submit() {
    if (this.form.valid) {
      of(this.form.value as User)
        .pipe(
          mergeMap((user) => this.authService.uploadProfile(user)),

          tap((user) => {
            if (this.imageFile) {
              return this.authService.uploadAvatar(this.imageFile);
            }
            return of(user);
          }),
          mergeMap(() => this.authService.getUserProfile()),
        )
        .subscribe(() => {
          this.modalRef.close({ movie: this.form.value });
        });
    }
  }
}
